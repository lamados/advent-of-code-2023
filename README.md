# Advent of Code 2023

Using it as an excuse to learn Erlang for some reason.

## License

It's [GNU AGPL](/LICENSE.txt) because if you use any of the code in this repo you should be ashamed and be legally
obligated to out yourself as someone who used this code.
