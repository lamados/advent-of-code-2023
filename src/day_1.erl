%% @doc Day 1 of Advent of Code 2023.
-module(day_1).
-export([solve/1]).

solve(Document) ->
  Lines = string:tokens(Document, "\n"),
  process_lines(Lines).

process_lines(List) ->
  process_lines(List, 0).
process_lines([Head | Tail], Sum) ->
  CalibrationValue = first_digit(Head, false) + first_digit(string:reverse(Head), true), %% calculate the calibration value.
  process_lines(Tail, Sum + CalibrationValue); %% recurse, adding the calibration value to the sum.
process_lines([], Sum) ->
  Sum. %% if there's no more elements, return the sum.

%% dont look at it this is the worst code ive ever written
first_digit([], _) -> 0;
first_digit([Head | _], true) when Head >= $0, Head =< $9 ->
  Head - 48;
first_digit([Head | _], false) when Head >= $0, Head =< $9 ->
  10 * (Head - 48);
first_digit([Head | Tail], false) when Head == $o ->
  case has_prefix(Tail, "ne") of
    true -> 10;
    false -> first_digit(Tail, false)
  end;
first_digit([Head | Tail], false) when Head == $t ->
  case has_prefix(Tail, "wo") of
    true -> 20;
    false -> case has_prefix(Tail, "hree") of
               true -> 30;
               false -> first_digit(Tail, false)
             end
  end;
first_digit([Head | Tail], false) when Head == $f ->
  case has_prefix(Tail, "our") of
    true -> 40;
    false -> case has_prefix(Tail, "ive") of
               true -> 50;
               false -> first_digit(Tail, false)
             end
  end;
first_digit([Head | Tail], false) when Head == $s ->
  case has_prefix(Tail, "ix") of
    true -> 60;
    false -> case has_prefix(Tail, "even") of
               true -> 70;
               false -> first_digit(Tail, false)
             end
  end;
first_digit([Head | Tail], false) when Head == $e ->
  case has_prefix(Tail, "ight") of
    true -> 80;
    false -> first_digit(Tail, false)
  end;
first_digit([Head | Tail], false) when Head == $n ->
  case has_prefix(Tail, "ine") of
    true -> 90;
    false -> first_digit(Tail, false)
  end;
first_digit([Head | Tail], true) when Head == $e ->
  case has_prefix(Tail, "no") of
    true -> 1;
    false -> case has_prefix(Tail, "erht") of
               true -> 3;
               false -> case has_prefix(Tail, "vif") of
                          true -> 5;
                          false -> case has_prefix(Tail, "nin") of
                                     true -> 9;
                                     false -> first_digit(Tail, true)
                                   end
                        end
             end
  end;
first_digit([Head | Tail], true) when Head == $o ->
  case has_prefix(Tail, "wt") of
    true -> 2;
    false -> first_digit(Tail, true)
  end;
first_digit([Head | Tail], true) when Head == $r ->
  case has_prefix(Tail, "uof") of
    true -> 4;
    false -> first_digit(Tail, true)
  end;
first_digit([Head | Tail], true) when Head == $x ->
  case has_prefix(Tail, "is") of
    true -> 6;
    false -> first_digit(Tail, true)
  end;
first_digit([Head | Tail], true) when Head == $n ->
  case has_prefix(Tail, "eves") of
    true -> 7;
    false -> first_digit(Tail, true)
  end;
first_digit([Head | Tail], true) when Head == $t ->
  case has_prefix(Tail, "hgie") of
    true -> 8;
    false -> first_digit(Tail, true)
  end;
first_digit([_ | Tail], Reverse) ->
  first_digit(Tail, Reverse). %% continue iterating over string.

%% this function is normal :)
has_prefix(_, Prefix) when Prefix == ""
  -> true;
has_prefix(String, _) when String == ""
  -> false;
has_prefix(String, Prefix) when length(Prefix) > length(String) -> false;
has_prefix(String, Prefix) when length(Prefix) == length(String) -> string:equal(String, Prefix);
has_prefix(String, Prefix) ->
  Trimmed = string:prefix(String, Prefix),
  Trimmed /= nomatch.